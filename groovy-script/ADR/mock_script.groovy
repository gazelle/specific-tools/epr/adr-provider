// create XmlHolder for request content
import org.apache.commons.lang.StringUtils;
import groovy.sql.Sql


try {
    def project = mockRequest.getContext().getMockService().getProject()
    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)

    //connection to ADR dataBase
    def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/adr", "gazelle", "gazelle", "org.postgresql.Driver")
    def testCase = project.testSuites['Library'].testCases['assertionValidation TestCase']

    // Action NotApplicable
    def notApplicableAction = []
    notApplicableAction.add("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b")
    notApplicableAction.add("urn:ihe:iti:2007:RetrieveDocumentSet")
    notApplicableAction.add("urn:ihe:rad:2009:RetrieveImagingDocumentSet")
    notApplicableAction.add("urn:ihe:iti:2007:CrossGatewayQuery")
    notApplicableAction.add("urn:ihe:iti:2007:CrossGatewayRetrieve")
    notApplicableAction.add("urn:ihe:rad:2011:CrossGatewayRetrieveImagingDocumentSet")

    // Action PPQ
    def action_ppq = []
    action_ppq.add("urn:e-health-suisse:2015:policy-administration:AddPolicy")
    action_ppq.add("urn:e-health-suisse:2015:policy-administration:UpdatePolicy")
    action_ppq.add("urn:e-health-suisse:2015:policy-administration:DeletePolicy")
    action_ppq.add("urn:e-health-suisse:2015:policy-administration:PolicyQuery")

    // Action XDS WRITING
    def actionWriting = []
    actionWriting.add("urn:ihe:iti:2007:RegisterDocumentSet-b")
    actionWriting.add("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b")
    actionWriting.add("urn:ihe:iti:2010:UpdateDocumentSet")
    actionWriting.add("urn:ihe:iti:2018:RestrictedUpdateDocumentSet")

    // Action XDS READING
    def actionReading = []
    actionReading.add("urn:ihe:iti:2007:RegistryStoredQuery")
    actionReading.add("urn:ihe:iti:2007:RetrieveDocumentSet")
    actionReading.add("urn:ihe:iti:2007:CrossGatewayQuery")
    actionReading.add("urn:ihe:iti:2007:CrossGatewayRetrieve")
    actionReading.add("urn:ihe:rad:2011:CrossGatewayRetrieveImagingDocumentSet")
    actionReading.add("urn:ihe:rad:2009:RetrieveImagingDocumentSet")

    // ACTION XDS
    def action_xds = []
    action_xds.add("urn:ihe:iti:2007:RegistryStoredQuery")
    action_xds.add("urn:ihe:iti:2007:RegisterDocumentSet-b")
    action_xds.add("urn:ihe:iti:2007:RetrieveDocumentSet")
    action_xds.add("urn:ihe:iti:2007:CrossGatewayQuery")
    action_xds.add("urn:ihe:iti:2007:CrossGatewayRetrieve")
    action_xds.add("urn:ihe:rad:2009:RetrieveImagingDocumentSet")
    action_xds.add("urn:ihe:rad:2011:CrossGatewayRetrieveImagingDocumentSet")
    action_xds.add("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b")
    action_xds.add("urn:ihe:iti:2010:UpdateDocumentSet")
    action_xds.add("urn:ihe:iti:2018:RestrictedUpdateDocumentSet")

    // ACTION ATC
    def action_atc = []
    action_atc.add("urn:e-health-suisse:2015:patient-audit-administration:RetrieveAtnaAudit")

    // Regex
    String reg_oid = "urn:oid:(([a-zA-Z0-9_.-])+.)*([a-zA-Z0-9])+"
    String oid = "(([0-9.])+.)*([0-9])+"

    // get argument
    ///// Security XUA SAML Assertion Properties
    ///////////////////
    def assertion = holder.getDomNode("//*:Header/*:Security/*:Assertion").toString()
    def assertionSubjectRoleCode = holder["//*:Security/*:Assertion/*:AttributeStatement/*:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:subject:role']/*:AttributeValue/*:Role/@code"]
    def assertionSubjectNameId = holder["//*:Security/*:Assertion/*:Subject/*:NameID"]
    def assertionSubjectResourceId = holder["//*:Security/*:Assertion/*:AttributeStatement/*:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:resource:resource-id']/*:AttributeValue"]

    def subjectId = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:subject:subject-id']/*:AttributeValue"]
    def subjectIdQualifier = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier']/*:AttributeValue"]
    def subjectHomeCommunityId = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:ihe:iti:xca:2010:homeCommunityId']/*:AttributeValue"]
    def subjectOrganisationId = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xspa:1.0:subject:organization-id']/*:AttributeValue"]
    def subjectRole = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:2.0:subject:role']/*:AttributeValue/hl7:CodedValue/@code"]
    def subjectCodeSystemRole = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:2.0:subject:role']/*:AttributeValue/hl7:CodedValue/@codeSystem"]
    def subjectDisplayNameRole = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:2.0:subject:role']/*:AttributeValue/hl7:CodedValue/@displayName"]
    def subjectPurposeofuseDisplayName = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xspa:1.0:subject:purposeofuse']/*:AttributeValue/hl7:CodedValue/@displayName"]
    def subjectPurposeofuseCode = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xspa:1.0:subject:purposeofuse']/*:AttributeValue/hl7:CodedValue/@code"]
    def subjectPurposeofuseCodeSystem = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Subject/*:Attribute[@AttributeId='urn:oasis:names:tc:xspa:1.0:subject:purposeofuse']/*:AttributeValue/hl7:CodedValue/@codeSystem"]

    def resourceId = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Resource//*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']//*:AttributeValue"]
    def resourceExtensionId = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Resource//*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']//*:AttributeValue//*:InstanceIdentifier/@extension"]
    def resourceRootId = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Resource//*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']//*:AttributeValue//*:InstanceIdentifier/@root"]

    def actionId = holder["//*:XACMLAuthzDecisionQuery/*:Request/*:Action/*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:action:action-id']/*:AttributeValue"]

    ////////// check assertion SAML
    ///////////////////////

    //    /// check checkAssertionSaml
    //    if (!checkAssertionSaml(holder, assertion)){
    //      return "mock_serverFault_generalResponse"
    //    }

    ////////// Subject Part
    ///////////////////////

    /// check subject_id
    if (!checkSubjectId(subjectId)) {
        return "mock_serverFault_generalResponse"
    }

    /// check subject_id_qualifier
    if (!checkSubjectIdQualifier(subjectIdQualifier)) {
        return "mock_serverFault_generalResponse"
    }

    /// check homeCommunityId
    if (!checkHomeCommunity(subjectHomeCommunityId, reg_oid)) {
        return "mock_serverFault_generalResponse"
    }

    /// check role
    if (!checkRole(subjectRole)) {
        return "mock_serverFault_generalResponse"
    }

    /// check code_system_role
    if (!checkCodeSytemRole(subjectCodeSystemRole, oid)) {
        return "mock_serverFault_generalResponse"
    }

    /// check display_name_role
    if (!checkDisplayNameRole(subjectDisplayNameRole)) {
        return "mock_serverFault_generalResponse"
    }

    /// check purposeofuse_code
    if (!checkPurposeCode(subjectPurposeofuseCode)) {
        return "mock_serverFault_generalResponse"
    }

    /// check purposeofuse_code_system
    if (!checkPurposeCodeSystem(subjectPurposeofuseCodeSystem, oid)) {
        return "mock_serverFault_generalResponse"
    }

    /// check purposeofuse_display_name
    if (!checkPurposeDisplayName(subjectPurposeofuseDisplayName)) {
        return "mock_serverFault_generalResponse"
    }

    /// check action_id
    if (!checkActionId(actionId)) {
        return "mock_serverFault_generalResponse"
    }

    //////////////////////////////////////
    //////////////////////////////////////

    ///// Manage request type
    /////////////////////////

    //// FOR INDETERMINATE
    if (!checkResourceAccess(holder, resourceId, resourceExtensionId)) {
        indeterminateResponse(holder, subjectHomeCommunityId, resourceId)
        return "ADR_indeterminate_generalResponse"
    }

    //// FOR ATC
    for (atc_action in action_atc) {
        // check action ATC
        if (atc_action.equalsIgnoreCase(actionId)) {
            if (performATC(holder, resourceId)) {
                atcResponse(subjectId, subjectRole, resourceExtensionId, subjectHomeCommunityId, resourceId, sql)

                return "ADR_due_to_ATC_generalSuccess"
            } else {
                return "mock_serverFault_generalResponse"
            }
        }
    }

    //// FOR PPQ
    for (ppq_action in action_ppq) {
        // check action PPQ
        if (ppq_action.equalsIgnoreCase(actionId)) {
            if (performPPQRequest(holder, actionId, subjectId, subjectHomeCommunityId)) {
                ppqResponse(sql, holder, subjectId, subjectRole, resourceExtensionId, resourceRootId, subjectHomeCommunityId, resourceId, actionId)
                return "ADR_due_to_PPQ_generalSuccess"
            } else {
                return "mock_serverFault_generalResponse"
            }
        }
    }

    //// FOR XDS
    for (xds_action in action_xds) {
        if (xds_action.equalsIgnoreCase(actionId)) {
            // check action XDS
            if (performADR(holder)) {
                xdsResponse(holder, subjectHomeCommunityId, resourceId, actionId, subjectRole, subjectId, actionReading, actionWriting, notApplicableAction, sql)
                return "ADR_due_to_XDS_generalSuccess"
            } else {
                return "mock_serverFault_generalResponse"
            }
        }
    }

    indeterminateResponse(holder, subjectHomeCommunityId, resourceId)
    return "ADR_indeterminate_generalResponse"

} catch (Exception e) {
    context.mockService.setPropertyValue('soapFaultCodeValue', "")
    context.mockService.setPropertyValue('soapFaultReason', "An error occurred while processing your request")
    return "mock_serverFault_generalResponse"
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////// MANAGE ADR_DUE_TO_XDS
/////////////////////////////////////////////
/////////////////////////////////////////////

////// Check XDS properties
////////////////////////////
def performADR(def holder) {

    def retrieveRequest = holder.getDomNode("//*:Request")
    String requestString = retrieveRequest.toString()
    int countResource = StringUtils.countMatches(requestString, "resource-id")

    if (countResource != 3) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Resource")
        context.mockService.setPropertyValue('soapFaultReason', "there are always exactly three Resources to be identified, each representing a class of documents: normal, restrictedandsecret documents.")
        return false
    }

    for (int i = 1; i <= 3; i++) {

        def root = holder["//*:Resource[".concat(i.toString()).concat("]/*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']/*:AttributeValue/hl7:InstanceIdentifier/@root")]
        def extension = holder["//*:Resource[".concat(i.toString()).concat("]/*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']/*:AttributeValue/hl7:InstanceIdentifier/@extension")]
        def confidentiality_code = holder["//*:Resource[".concat(i.toString()).concat("]/*:Attribute[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/*:AttributeValue/hl7:CodedValue/@code")]
        def confidentiality_code_system = holder["//*:Resource[".concat(i.toString()).concat("]/*:Attribute[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/*:AttributeValue/hl7:CodedValue/@codeSystem")]
        def resource_id = holder["//*:Resource[".concat(i.toString()).concat("]/*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']/*:AttributeValue")]
        def confidentiality_code_display = holder["//*:Resource[".concat(i.toString()).concat("]/*:Attribute[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']/*:AttributeValue/hl7:CodedValue/@displayName")]


        if (!root.matches("(([0-9.])+.)*([0-9])+")) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Attribute[urn:e-health-suisse:2015:epr-spid]:AttributeValue:hl7:InstanceIdentifier@root")
            context.mockService.setPropertyValue('soapFaultReason', "The root attribute of this element SHALL NOT be empty and follow OID syntax, e.g. 2.16.756.5.30.1")
            return false
        }

        if (!extension?.trim()) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Attribute[urn:e-health-suisse:2015:epr-spid]:AttributeValue:hl7:InstanceIdentifier@extension")
            context.mockService.setPropertyValue('soapFaultReason', "The extension attribute of this element SHALL NOT be empty or null and SHALL be equal to the patient ID")
            return false
        }

        if (!confidentiality_code?.trim()) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Atribute[urn:ihe:iti:xds-b:2007:confidentiality-code]:AttributeValue:hl7@code")
            context.mockService.setPropertyValue('soapFaultReason', "The code attribute of this element SHALL NOT be empty or null")
            return false
        }

        if (!confidentiality_code_system.matches("(([0-9.])+.)*([0-9])+")) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Attribute[urn:ihe:iti:xds-b:2007:confidentiality-code]:AttributeValue:hl7@codeSystem")
            context.mockService.setPropertyValue('soapFaultReason', "The codeSystem attribute of this element SHALL NOT be empty and follow OID syntax, e.g. 2.16.756.5.30.1")
            return false
        }

        if (!resource_id.contains(extension)) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Attribute[urn:oasis:names:tc:xacml:1.0:resource:resource-id]:AttributeValue")
            context.mockService.setPropertyValue('soapFaultReason', "The AttributeValue of this element SHALL be contains the patient ID, the extension '" + extension + "' is not present in '" + resource_id + "'")
            return false
        }

        if (!confidentiality_code_display.equalsIgnoreCase("normal")
                && !confidentiality_code_display.equalsIgnoreCase("restricted")
                && !confidentiality_code_display.equalsIgnoreCase("secret")) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource/Attribute[urn:ihe:iti:xds-b:2007:confidentiality-code]/AttributeValue/hl7:CodedValue/@displayName")
            context.mockService.setPropertyValue('soapFaultReason', "Failed to provide a valid response. Your input is probably different than in the simulation specs (i.e. normal, restricted or secret), please check it again and repeat the request.")
            return false
        }
    }
    return true
}

///////// xdsResponse
/////////////////////
def xdsResponse(
        def holder,
        def communityID,
        def idResource,
        def actionId,
        def role, def subjectId, def actionReading, def actionWriting, def notApplicableActionList, def sql) {

    def listResult = ""
    def issueInstance = new Date().format("yyyy-MM-dd") + "T" + new Date().format("HH:mm:ss.S") + "Z"
    def statusCode = "urn:oasis:names:tc:SAML:2.0:status:Success"
    def decision = "Deny"


    String result = '<xacml-context:Result ResourceId="__id_resource__">' +
            '<xacml-context:Decision>__decision__</xacml-context:Decision>' +
            '<xacml-context:Status>' +
            '<xacml-context:StatusCode Value="__Status_Code__" />' +
            '</xacml-context:Status>' +
            '</xacml-context:Result>'
    int i = 1
    for (id in idResource) {
        root_id = holder["//*:Resource[" + i + "]//*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']//*:AttributeValue//*:InstanceIdentifier/@root"]
        extension_id = holder["//*:Resource[" + i + "]//*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']//*:AttributeValue//*:InstanceIdentifier/@extension"]
        displayName = holder["//*:Resource[" + i + "]//*:Attribute[@AttributeId='urn:ihe:iti:xds-b:2007:confidentiality-code']//*:AttributeValue//*:CodedValue/@displayName"]


        if (patientDataDecision(extension_id, root_id, communityID)) {
            decision = xdsAdrDecision(actionId, role, actionReading, actionWriting, notApplicableActionList, extension_id, communityID, subjectId, displayName, sql)
        }

        listResult = listResult + result.replace("__id_resource__", id)
        listResult = listResult.replace("__decision__", decision)
        listResult = listResult.replace("__Status_Code__", "urn:oasis:names:tc:xacml:1.0:status:ok")
        i++
    }
    context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid3', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('result', listResult)
    context.mockService.setPropertyValue('Status_Code', statusCode)
    context.mockService.setPropertyValue('community', communityID)
    context.mockService.setPropertyValue('issueInstance', issueInstance)
}

///////// MANAGE ADR_DUE_TO_ATC
/////////////////////////////////////////////
/////////////////////////////////////////////
def performATC(def holder, def resourceId) {

    def retrieveRequest = holder.getDomNode("//*:Request")
    String requestString = retrieveRequest.toString()
    int countResource = StringUtils.countMatches(requestString, "resource-id");

    if (countResource != 1) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Resource")
        context.mockService.setPropertyValue('soapFaultReason', "There must have exactly one Resource")
        return false
    }

    if (!resourceId.contains(':patient-audit-trail-records')) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Attribute[urn:oasis:names:tc:xacml:1.0:resource:resource-id]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "For ADR due to CH:ATC there is exactly one Resource to be identified")
        return false;
    } else {
        return true;
    }
}

//////// atcResponse
////////////////////
def atcResponse(
        def subjectId,
        def subjectRole,
        def resourceExtensionId, def subjectHomeCommunityId, def resourceId, def sql) {

    def issueInstance = new Date().format("yyyy-MM-dd") + "T" + new Date().format("HH:mm:ss.S") + "Z"
    def statusCode = "urn:oasis:names:tc:SAML:2.0:status:Success"
    def decision = "Deny"

    String result = '<xacml-context:Result ResourceId="__id_resource__">' +
            '<xacml-context:Decision>__decision__</xacml-context:Decision>' +
            '<xacml-context:Status>' +
            '<xacml-context:StatusCode Value="__Status_Code__" />' +
            '</xacml-context:Status>' +
            '</xacml-context:Result>'


    if (subjectIdAccess(subjectId)) {
        decision = atcAdrDecision(subjectRole, resourceExtensionId, subjectHomeCommunityId, subjectId, sql)
    }

    def listResult = result.replace("__id_resource__", resourceId)
    listResult = listResult.replace("__decision__", decision)
    listResult = listResult.replace("__Status_Code__", "urn:oasis:names:tc:xacml:1.0:status:ok")

    context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid3', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('result', listResult)
    context.mockService.setPropertyValue('Status_Code', statusCode)
    context.mockService.setPropertyValue('community', subjectHomeCommunityId)
    context.mockService.setPropertyValue('issueInstance', issueInstance)

}

///////// MANAGE ADR_DUE_TO_PPQ
/////////////////////////////////////////////
/////////////////////////////////////////////

//// Perform PPQ Request
///////////////////////
def performPPQRequest(def holder, def actionId, def subjectId, def subjectHomeCommunityId) {

    def retrieveRequest = holder.getDomNode("//*:Request")
    String requestString = retrieveRequest.toString()
    int countResource = StringUtils.countMatches(requestString, "resource:resource-id")

    for (int x = 1; x <= countResource; x++) {

        def id = holder["//*:Resource[" + x + "]//*:Attribute[@AttributeId='urn:oasis:names:tc:xacml:1.0:resource:resource-id']//*:AttributeValue"]
        def extension = holder["//*:Resource[" + x + "]/*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']//*:AttributeValue/hl7:InstanceIdentifier/@extension"]
        def referenced_policy_set = holder["//*:Resource[" + x + "]/*:Attribute[@AttributeId='urn:e-health-suisse:2015:policy-attributes:referenced-policy-set']//*:AttributeValue"]

        if (id.contains('urn:uuid:')) {
            id = id.replace('urn:uuid:', '')
        }

        if (!id.matches("([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})+")) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Attribute[urn:e-health-suisse:2015:epr-spid]:AttributeValue")
            context.mockService.setPropertyValue('soapFaultReason', "This element SHALL have a value that follows UUID syntax, e.g. c969c7cd-9fe9-4fdc-83c5-a7b5118922a3")
            return false;
        }

        if (!extension?.trim()) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource:Attribute[urn:e-health-suisse:2015:epr-spid]:AttributeValue:hl7@extension")
            context.mockService.setPropertyValue('soapFaultReason', "The extension attribute of this element SHALL NOT be empty or null and SHALL be equal to the patient ID")
            return false;
        }

        if (!referenced_policy_set.equalsIgnoreCase("urn:e-health-suisse:2015:policies:exclusion-list")) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Resource/Attribute[urn:e-health-suisse:2015:policy-attributes:referenced-policy-set]/AttributeValue")
            context.mockService.setPropertyValue('soapFaultReason', "Failed to provide a valid response. Your input is probably different than in the simulation specs (i.e. urn:e-health-suisse:2015:policies:exclusion-list), please check it again and repeat the request.")
            return false;
        }
    }

    if (actionId.equalsIgnoreCase("urn:e-health-suisse:2015:policy-administration:AddPolicy")) {
        if (!subjectId?.trim()) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject-id]:AttributeValue")
            context.mockService.setPropertyValue('soapFaultReason', "This element SHALL NOT be empty or null and SHALL be equal to the patient ID")
            return false
        } else {
            context.mockService.setPropertyValue('community', subjectHomeCommunityId)
            context.mockService.setPropertyValue('decision', "Permit")
            context.mockService.setPropertyValue('statusMessage', "Add Policy")
            return true
        }
    } else if (actionId.equalsIgnoreCase("urn:e-health-suisse:2015:policy-administration:UpdatePolicy")) {

        if (!subjectId?.trim()) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject-id]:AttributeValue")
            context.mockService.setPropertyValue('soapFaultReason', "This element SHALL NOT be empty or null and SHALL be equal to the patient ID")
            return false
        } else {
            context.mockService.setPropertyValue('community', subjectHomeCommunityId)
            context.mockService.setPropertyValue('decision', "Permit")
            context.mockService.setPropertyValue('statusMessage', "Update Policy")
            return true
        }

    } else if (actionId.equalsIgnoreCase("urn:e-health-suisse:2015:policy-administration:DeletePolicy")) {
        if (!subjectId?.trim()) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject-id]:AttributeValue")
            context.mockService.setPropertyValue('soapFaultReason', "This element SHALL NOT be empty or null and SHALL be equal to the patient ID")
            return false;
        } else {
            context.mockService.setPropertyValue('community', subjectHomeCommunityId)
            context.mockService.setPropertyValue('decision', "Permit")
            context.mockService.setPropertyValue('statusMessage', "Delete Policy")
            return true;
        }

    } else if (actionId.equalsIgnoreCase("urn:e-health-suisse:2015:policy-administration:PolicyQuery")) {
        if (!subjectId?.trim()) {
            context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject-id]:AttributeValue")
            context.mockService.setPropertyValue('soapFaultReason', "This element SHALL NOT be empty or null and SHALL be equal to the patient ID")
            return false;
        } else {
            context.mockService.setPropertyValue('community', subjectHomeCommunityId)
            context.mockService.setPropertyValue('decision', "Permit")
            context.mockService.setPropertyValue('statusMessage', "Policy Query")
            return true;
        }

    } else {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Action:Attribute[urn:oasis:names:tc:xacml:1.0:action-id]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "The AttributeValue for action id is not properly defined")
        return false;
    }
}

//// PPQ Response
///////////////////////
def ppqResponse(
        def sql,
        def holder,
        def subjectId,
        def subjectRole,
        def resourceExtensionId, def resourceRootId, def subjectHomeCommunityId, def resourceId, def actionId) {

    def issueInstance = new Date().format("yyyy-MM-dd") + "T" + new Date().format("HH:mm:ss.S") + "Z"
    def statusCode = "urn:oasis:names:tc:SAML:2.0:status:Success"
    def decision = "Deny"
    int i = 1
    def listResult = ""

    String result = '<xacml-context:Result ResourceId="__id_resource__">' +
            '<xacml-context:Decision>__decision__</xacml-context:Decision>' +
            '<xacml-context:Status>' +
            '<xacml-context:StatusCode Value="__Status_Code__" />' +
            '</xacml-context:Status>' +
            '</xacml-context:Result>'

    def retrieveRequest = holder.getDomNode("//*:Request")
    String requestString = retrieveRequest.toString()
    int countResource = StringUtils.countMatches(requestString, "resource-id");


    if (countResource.toInteger() == 1) {
        def full_resource = "urn:uuid:" + resourceId
        if (subjectIdAccess(subjectId)) {
            decision = ppqAdrDecision(subjectRole, resourceExtensionId, subjectHomeCommunityId, subjectId, full_resource, sql)
        }
        listResult = result.replace("__id_resource__", resourceId)
        listResult = listResult.replace("__decision__", decision)
        listResult = listResult.replace("__Status_Code__", "urn:oasis:names:tc:xacml:1.0:status:ok")

    } else {
        for (id in resourceId) {
            def full_resource = "urn:uuid:" + id
            resourceExtensionId = holder["//*:Resource[" + i + "]//*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']//*:AttributeValue//*:InstanceIdentifier/@extension"]
            resourceRootId = holder["//*:Resource[" + i + "]//*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']//*:AttributeValue//*:InstanceIdentifier/@root"]
            decision = ppqAdrDecision(subjectRole, resourceExtensionId, subjectHomeCommunityId, subjectId, full_resource, sql)

            listResult = listResult + result.replace("__id_resource__", id)
            listResult = listResult.replace("__decision__", decision)
            listResult = listResult.replace("__Status_Code__", "urn:oasis:names:tc:xacml:1.0:status:ok")
            i++
        }
    }
    context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid3', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('result', listResult)
    context.mockService.setPropertyValue('Status_Code', statusCode)
    context.mockService.setPropertyValue('community', subjectHomeCommunityId)
    context.mockService.setPropertyValue('issueInstance', issueInstance)
}

///////// MANAGE INDETERMINATE RESPONSE
/////////////////////////////////////////////
/////////////////////////////////////////////

///// Prepare indeterminate response
/////////////////////////////////////
def indeterminateResponse(def holder, def subjectHomeCommunityId, def resourceId) {

    def issueInstance = new Date().format("yyyy-MM-dd") + "T" + new Date().format("HH:mm:ss.S") + "Z"
    def statusCode = "urn:e-health-suisse:2015:error:not-holder-of-patient-policies"
    def decision = "Indeterminate"
    def listResult = ""

    String result = '<xacml-context:Result ResourceId="__id_resource__">' +
            '<xacml-context:Decision>__decision__</xacml-context:Decision>' +
            '<xacml-context:Status>' +
            '<xacml-context:StatusCode Value="__Status_Code__" />' +
            '</xacml-context:Status>' +
            '</xacml-context:Result>'


    def retrieveRequest = holder.getDomNode("//*:Request")
    String requestString = retrieveRequest.toString()
    int countResource = StringUtils.countMatches(requestString, "resource-id");

    if (countResource == 1) {
        listResult = result.replace("__id_resource__", resourceId)
        listResult = listResult.replace("__decision__", decision)
        listResult = listResult.replace("__Status_Code__", "urn:e-health-suisse:2015:error:not-holder-of-patient-policies")
    } else {
        for (id in resourceId) {
            listResult = listResult + result.replace("__id_resource__", id)
            listResult = listResult.replace("__decision__", decision)
            listResult = listResult.replace("__Status_Code__", "urn:e-health-suisse:2015:error:not-holder-of-patient-policies")
        }
    }

    context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid2', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('uuid3', UUID.randomUUID().toString())
    context.mockService.setPropertyValue('result', listResult)
    context.mockService.setPropertyValue('Status_Code', statusCode)
    context.mockService.setPropertyValue('community', subjectHomeCommunityId)
    context.mockService.setPropertyValue('issueInstance', issueInstance)
}

///// Check if resource id is indeterminate
/////////////////////////////////////////
def checkResourceAccess(def holder, def resourceId, def resourceExtensionId) {

    def retrieveRequest = holder.getDomNode("//*:Request")
    String requestString = retrieveRequest.toString()
    int countResource = StringUtils.countMatches(requestString, "resource-id");
    int i = 1;

    if (countResource == 1) {
        if (subjectIdAccess(resourceExtensionId)) {
            if (!resourceId.matches("([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})+")) {
                if (resourceId.contains(resourceExtensionId)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    } else {

        for (id in resourceId) {
            resourceExtensionId = holder["//*:Resource[" + i + "]/*:Attribute[@AttributeId='urn:e-health-suisse:2015:epr-spid']/*:AttributeValue/hl7:InstanceIdentifier/@extension"]
            if (subjectIdAccess(resourceExtensionId)) {
                if (!id.matches("([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})+")) {
                    if (!id.contains(resourceExtensionId)) {
                        return false;
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
            i++
        }
        return true;
    }
}

////// CHECK PROPERTIES

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//// SAML Assertion is valid
////////////////////////////
def checkAssertionSaml(def holder, def assertion) {

    if (assertion == 'null') {
        context.mockService.setPropertyValue('soapFaultReason', "Missing SAML Assertion")
        return false;
    }
    assertion = assertion.replace('<?xml version="1.0" encoding="UTF-8"?>', '')
    testCase.setPropertyValue('assertionDocument', assertion)

    def testCaseContext = new com.eviware.soapui.support.types.StringToObjectMap();
    def runner = testCase.run(testCaseContext, false)
    def getValidationAssertion = testCase.getPropertyValue("validationResponse")
    def getErrorMessage = testCase.getPropertyValue("errorMessage")

    if (!getValidationAssertion.contains('<ValidationTestResult>PASSED</ValidationTestResult>')) {
        context.mockService.setPropertyValue('soapFaultReason', "Invalid SAML Assertion")
        requestContext.soapFaultError = getErrorMessage
        return false;
        //return "mock_serverFault_generalResponse"
    } else {
        return true;
    }
}

///// Check if the PolicySet exist
///////////////////////////
def retrievePolicySet(def sql, def policySetId, def extension, def root, def community) {
    def SQL = "Select count(*) as nbr_policy from policy_set_stack where community like ? and id_root like ? and id_extension like ? and policyset_id like ? and is_active = true;"
    sql.eachRow(SQL, [community, root, extension, policySetId]) { p -> nbr_policy = "${p.nbr_policy}" }

    if (nbr_policy >= 1) {
        return true
    } else {
        return false
    }
}

//// check if the subject id is not null
////////////////////////////////////////
def checkSubjectId(def subject_id) {

    if (subject_id == "" || subject_id == null || subject_id.toString() == '[]') {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject-id]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element 'Subject:Attribute[subject-id]:AttributeValue' SHALL have a value")
        return false;
    } else {
        return true;
    }
}

///// check subject_id_qualifier
////////////////////////////////
def checkSubjectIdQualifier(def subject_id_qualifier) {

    if (subject_id_qualifier == "" || subject_id_qualifier == null || subject_id_qualifier.toString() == '[]') {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject_id_qualifier]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element 'Subject:Attribute[subject_id_qualifier]:AttributeValue' SHALL have a value")
        return false;
    }

    if (!subject_id_qualifier.equalsIgnoreCase("urn:e-health-suisse:policy-administrator-id") && !subject_id_qualifier.equalsIgnoreCase("urn:gs1:gln") && !subject_id_qualifier.equalsIgnoreCase("urn:e-health-suisse:2015:epr-spid") && !subject_id_qualifier.equalsIgnoreCase("urn:e-health-suisse:representative-id") && !subject_id_qualifier.equalsIgnoreCase("urn:e-health-suisse:document-administrator-id")) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject-id-qualifier]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element SHALL have a value equal to urn:e-health-suisse:2015:epr-spid in case of a patient or guardian or urn:gs1:gln in case of a professional or auxiliary person")
        return false;
    } else {
        return true;
    }

}

/// check homeCommunityId
/////////////////////////
def checkHomeCommunity(def homeCommunityId, def reg_oid) {

    if (homeCommunityId == "" || homeCommunityId == null || homeCommunityId.toString() == '[]') {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[homeCommunityId]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element 'Subject:Attribute[homeCommunityId]:AttributeValue' SHALL have a value")
        return false;
    }

    if (!homeCommunityId.matches(reg_oid)) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[homeCommunityId]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element SHALL have a value that follows URN syntax and is equal to the Home Community OID, e.g. urn:oid:2.999.1.1")
        return false;
    } else {
        return true;
    }
}

/// check role
//////////////
def checkRole(def role) {

    if (role == "" || role == null || role.toString() == '[]') {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[subject-role]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element 'Subject:Attribute[subject-role]:AttributeValue' SHALL have a value")
        return false;
    }

    if (!role?.trim()) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[role]:AttributeValue:hl7@code")
        context.mockService.setPropertyValue('soapFaultReason', "The code attribute of this element SHALL NOT be empty or null")
        return false
    } else {
        return true
    }
}

/// check code_system_role
//////////////////////////
def checkCodeSytemRole(def code_system_role, def oid) {

    if (!code_system_role.matches(oid)) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[role]:AttributeValue:hl7@codeSystem")
        context.mockService.setPropertyValue('soapFaultReason', "The codeSystem attribute of this element SHALL NOT be empty and follow OID syntax, e.g. 2.16.756.5.30.1")
        return false
    } else {
        return true
    }
}

/// check display_name_role
//////////////////////////
def checkDisplayNameRole(def display_name_role) {

    if (!display_name_role?.trim()) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[role]:AttributeValue:hl7@displayName")
        context.mockService.setPropertyValue('soapFaultReason', "The displayName attribute of this element SHALL NOT be empty or null")
        return false;
    } else {
        return true;
    }

}

/// check purposeofuse_code
///////////////////////////
def checkPurposeCode(def purposeofuse_code) {
    // vi. Subject:Atribute[purposeofuse]:AttributeValue
    if (purposeofuse_code == "" || purposeofuse_code == null || purposeofuse_code.toString() == '[]') {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[purposeofuse]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element 'Subject:Attribute[purposeofuse]:AttributeValue' SHALL have a value")
        return false
    }
    // vii. Subject:Atribute[purposeofuse]:AttributeValue:hl7@code
    if (!purposeofuse_code?.trim()) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Atribute[purposeofuse]:AttributeValue:hl7@code")
        context.mockService.setPropertyValue('soapFaultReason', "The code attribute of this element SHALL NOT be empty or null")
        return false
    } else {
        return true
    }

}

/// check purposeofuse_code_system
//////////////////////////////////
def checkPurposeCodeSystem(def purposeofuse_code_system, def oid) {
    // viii. Subject:Atribute[purposeofuse]:AttributeValue:hl7@codeSystem
    if (!purposeofuse_code_system.matches(oid)) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[purposeofuse]:AttributeValue:hl7@codeSystem")
        context.mockService.setPropertyValue('soapFaultReason', "The codeSystem attribute of this element SHALL NOT be empty and follow OID syntax, e.g. 2.16.756.5.30.1")
        return false;
    } else {
        return true;
    }
}

/// check purposeofuse_displayName
//////////////////////////////////
def checkPurposeDisplayName(def purposeofuse_display_name) {
    // ix. Subject:Atribute[purposeofuse]:AttributeValue:hl7@displayName
    if (!purposeofuse_display_name?.trim()) {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[purposeofuse]:AttributeValue:hl7@displayName")
        context.mockService.setPropertyValue('soapFaultReason', "The displayName attribute of this element SHALL NOT be empty or null")
        return false;
    } else {
        return true;
    }
}

/// check action_id
///////////////////
def checkActionId(def action_id) {
    // check action
    if (action_id == "" || action_id == null || action_id.toString() == '[]') {
        context.mockService.setPropertyValue('soapFaultCodeValue', "Subject:Attribute[action-id]:AttributeValue")
        context.mockService.setPropertyValue('soapFaultReason', "This element 'Action:Attribute[action-id]:AttributeValue' SHALL have a value")
        return false;
    } else {
        return true;
    }
}

//////////////////////////////////////////////////
///////// ADR DECISION FUNCTION
/////////////////////////////////////////////////////////

///// ADR due to XDS Decision
/////////////////////////////
def xdsAdrDecision(
        def actionId,
        def subjectRole,
        def actionReading,
        def actionWriting,
        def notApplicableActionList,
        def extensionId,
        def homeCommunity,
        def subject,
        def confidentiality,
        def sql) {


    for (action in notApplicableActionList) {
        if (actionId.contains(action)) {
            return "NotApplicable"
        }
    }

    if (subjectRole.contains("PAT") || subjectRole.contains("REP")) {
        def full = "SELECT count(*) as nbrFull from policy_set_stack where id_extension = ? and policysetidreference like '%full%' and community = ? and subject = ? and subject_role = ? and is_active = true;"
        request = sql.eachRow(full, [extensionId, homeCommunity, subject, subjectRole])
                { p -> nbrFull = "${p.nbrFull}" }

        if (nbrFull.toInteger() != 0) {
            return "Permit"
        } else {
            return "NotApplicable"
        }

    } else if (subjectRole.contains("DADM")) {
        if ((actionId.contains("urn:ihe:iti:2007:RegistryStoredQuery") || actionId.contains("urn:ihe:iti:2010:UpdateDocumentSet"))) {

            if (confidentiality.contains("normal") || confidentiality.contains("restricted")) {
                return "Permit"
            } else {
                return "Deny"
            }
        } else {
            return "Deny"
        }

    } else if (subjectRole.contains("HCP") || subjectRole.contains("ASS")) {


        for (access in actionReading) {
            if (access.contains(actionId)) {
                def normalAccess = "SELECT count(*) as normalAccess from policy_set_stack where id_extension = ? and policysetidreference like '%access-level:normal%' and community = ? and subject = ? and is_active = true;"
                def restrictedAccess = "SELECT count(*) as restrictedAccess from policy_set_stack where id_extension = ? and policysetidreference like '%access-level:restricted%' and community = ? and subject = ? and is_active = true;"
                def exclusionAccess = "SELECT count(*) as exclusionAccess from policy_set_stack where id_extension = ? and policysetidreference like '%exclusion-list%' and community = ? and subject = ? and is_active = true;"

                request = sql.eachRow(normalAccess, [extensionId, homeCommunity, subject]) { p -> normalAccess = "${p.normalAccess}" }
                request = sql.eachRow(restrictedAccess, [extensionId, homeCommunity, subject]) { p -> restrictedAccess = "${p.restrictedAccess}" }
                request = sql.eachRow(exclusionAccess, [extensionId, homeCommunity, subject]) { p -> exclusionAccess = "${p.exclusionAccess}" }


                if (normalAccess.toInteger() != 0 && confidentiality.contains("normal")) {
                    return "Permit"
                } else if (confidentiality.contains("restricted") && normalAccess.toInteger() != 0) {
                    return "Deny"
                } else if (confidentiality.contains("secret") && normalAccess.toInteger() != 0) {
                    return "Deny"


                } else if (restrictedAccess.toInteger() != 0 && confidentiality.contains("normal")) {
                    return "Permit"
                } else if (restrictedAccess.toInteger() != 0 && confidentiality.contains("restricted")) {
                    return "Permit"
                } else if (restrictedAccess.toInteger() != 0 && confidentiality.contains("secret")) {
                    return "Deny"

                } else if (exclusionAccess.toInteger() != 0) {
                    return "Deny"
                } else {
                    return "NotApplicable"
                }
            }
        }


        for (provide in actionWriting) {
            if (provide.contains(actionId)) {

                def secretProvide = "SELECT count(*) as secretProvide from policy_set_stack where id_extension = ? and policysetidreference like '%provide-level:secret%' and community = ? and subject = ? and is_active = true;"
                def restrictedProvide = "SELECT count(*) as restrictedProvide from policy_set_stack where id_extension = ? and policysetidreference like '%provide-level:restricted%' and community = ? and subject = ? and is_active = true;"
                def normalProvide = "SELECT count(*) as normalProvide from policy_set_stack where id_extension = ? and policysetidreference like '%provide-level:normal%' and community = ? and subject = ? and is_active = true;"

                request = sql.eachRow(secretProvide, [extensionId, homeCommunity, subject]) { p -> secretProvide = "${p.secretProvide}" }
                request = sql.eachRow(restrictedProvide, [extensionId, homeCommunity, subject]) { p -> restrictedProvide = "${p.restrictedProvide}" }
                request = sql.eachRow(normalProvide, [extensionId, homeCommunity, subject]) { p -> normalProvide = "${p.normalProvide}" }

                if (normalProvide.toInteger() != 0 && confidentiality.contains("normal")) {
                    return "Permit"
                } else if (normalProvide.toInteger() != 0 && confidentiality.contains("restricted")) {
                    return "Permit"
                } else if (normalProvide.toInteger() != 0 && confidentiality.contains("secret")) {
                    return "Deny"

                } else if (restrictedProvide.toInteger() != 0 && confidentiality.contains("normal")) {
                    return "Deny"
                } else if (restrictedProvide.toInteger() != 0 && confidentiality.contains("restricted")) {
                    return "Permit"
                } else if (restrictedProvide.toInteger() != 0 && confidentiality.contains("secret")) {
                    return "Deny"

                } else if (secretProvide.toInteger() != 0 && confidentiality.contains("normal")) {
                    return "Deny"
                } else if (secretProvide.toInteger() != 0 && confidentiality.contains("restricted")) {
                    return "Deny"
                } else if (secretProvide.toInteger() != 0 && confidentiality.contains("secret")) {
                    return "Permit"
                } else {
                    return "NotApplicable"
                }
            }
        }
    } else {
        return "Deny"
    }

}

///// ADR due to ATC Decision
/////////////////////////////
def atcAdrDecision(
        def subjectRole,
        def extensionId,
        def homeCommunity,
        def subject,
        def sql) {

    if (subjectRole.contains("PAT") || subjectRole.contains("REP")) {
        def full = "SELECT count(*) as nbrFull from policy_set_stack where id_extension = ? and policysetidreference like '%full%' and community = ? and subject = ? and subject_role = ? and is_active = true;"
        request = sql.eachRow(full, [extensionId, homeCommunity, subject, subjectRole])
                { p -> nbrFull = "${p.nbrFull}" }

        if (nbrFull.toInteger() != 0) {
            return "Permit"
        } else {
            return "NotApplicable"
        }
    } else {
        return "Deny"
    }
}

///// ADR due to PPQ Decision
/////////////////////////////
def ppqAdrDecision(
        def subjectRole,
        def extensionId,
        def homeCommunity,
        def subject,
        def resourceId,
        def sql) {


    if (subjectRole.contains("PADM")) {
        def full = "SELECT count(*) as nbrFull from policy_set_stack where id_extension = ? and policysetidreference like '%policy-bootstrap%' and community = ? and subject_role = ? and policyset_id like ? and is_active = true;"
        request = sql.eachRow(full, [extensionId, homeCommunity, subjectRole, resourceId])
                { p -> nbrFull = "${p.nbrFull}" }

        if (nbrFull.toInteger() != 0) {
            return "Permit"
        } else {
            return "NotApplicable"
        }
    } else if (subjectRole.contains("PAT") || subjectRole.contains("REP")) {
        def full = "SELECT count(*) as nbrFull from policy_set_stack where id_extension = ? and policysetidreference like '%full%' and community = ? and subject = ? and subject_role = ? and policyset_id like ? and is_active = true;"
        request = sql.eachRow(full, [extensionId, homeCommunity, subject, subjectRole, resourceId])
                { p -> nbrFull = "${p.nbrFull}" }
        if (nbrFull.toInteger() != 0) {
            return "Permit"
        } else {
            return "NotApplicable"
        }
    } else if (subjectRole.contains("HCP") || subjectRole.contains("ASS")) {
        def delegation = "SELECT count(*) as nbrDelegation from policy_set_stack where id_extension = ? and policysetidreference like '%delegation%' and community = ? and subject = ? and subject_role = ? and policyset_id like ? and is_active = true;"
        request = sql.eachRow(delegation, [extensionId, homeCommunity, subject, subjectRole, resourceId])
                { p -> nbrDelegation = "${p.nbrDelegation}" }
        if (nbrDelegation.toInteger() != 0) {
            return "Permit"
        } else {
            return "NotApplicable"
        }
    } else {
        return "Deny"
    }
}

////////////////////////////////////////////////////////////////
//////////// PATIENT DATA
////////////////////////////////////////////////////////////////

///// Check resource access
///////////////////////////

///// Patient ID + Community ID
///////////////////////////////////
def patientDataDecision(def extension, def root, def community) {

    if (extension.equals('761337610435209810') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:2.2.2')) {
        return true
    } else if (extension.equals('761337610435200998') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610435209810') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610436974489') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610430891416') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610423590456') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610455909127') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610445502987') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610448027647') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610469261945') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610448027647') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610510635763') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else if (extension.equals('761337610433933946') && root.equals('2.16.756.5.30.1.127.3.10.3') && community.equals('urn:oid:1.3.6.1.4.1.21367.2017.2.6.2')) {
        return true
    } else {
        return false
    }
}

///// Subject ID
////////////////////
def subjectIdAccess(def subject_id) {
    if (subject_id.equals("7601000050717")) {
        return true
    } else if (subject_id.equals("7601002033572")) {
        return true
    } else if (subject_id.equals("7601002467458")) {
        return true
    } else if (subject_id.equals("7601002469191")) {
        return true
    } else if (subject_id.equals("7601002468963")) {
        return true
    } else if (subject_id.equals("7601002467373")) {
        return true
    } else if (subject_id.equals("7601002468282")) {
        return true
    } else if (subject_id.equals("7601002466565")) {
        return true
    } else if (subject_id.equals("7601002467158")) {
        return true
    } else if (subject_id.equals("7601002466812")) {
        return true
    } else if (subject_id.equals("7601002462586")) {
        return true
    } else if (subject_id.equals("7601002461111")) {
        return true
    } else if (subject_id.equals("761337610435200998")) {
        return true
    } else if (subject_id.equals("761337610435209810")) {
        return true
    } else if (subject_id.equals("761337610436974489")) {
        return true
    } else if (subject_id.equals("761337610430891416")) {
        return true
    } else if (subject_id.equals("761337610423590456")) {
        return true
    } else if (subject_id.equals("761337610455909127")) {
        return true
    } else if (subject_id.equals("761337610445502987")) {
        return true
    } else if (subject_id.equals("761337610448027647")) {
        return true
    } else if (subject_id.equals("761337610469261945")) {
        return true
    } else if (subject_id.equals("761337610510635763")) {
        return true
    } else if (subject_id.equals("761337610433933946")) {
        return true
    } else {
        return false
    }
}
